;;
;;  This file allows me to easily publish these notes (export them to html)
;;
;;; Code:

(require 'ox-publish)
(setq org-publish-project-alist
      '(
        ("org-notes"
         :base-directory "~/org-notes/org/"
         :base-extension "org"
         :publishing-directory "~/org-notes/public/"
         :publishing-function org-html-publish-to-html
         :headline-levels 4             ; Just the default for this project.
         :auto-preamble t)
        ("org-static"
         :base-directory "~/org-notes/org/"
         :base-extension "png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf\\|svg"
         :publishing-directory "~/org-notes/public/"
         :publishing-function org-publish-attachment)
        ("css"
         :base-directory "~/org-notes/style/"
         :base-extension "css\\|woff2"
         :publishing-directory "~/org-notes/public/style/"
         :recursive t
         :publishing-function org-publish-attachment)
        ("org" :components ("org-notes" "org-static" "css"))))

(org-publish "org")

(provide 'publish)
;;; publish.el ends here
