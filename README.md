# Mark's personal notes

goto [hosted notes](https://markmizzi.gitlab.io/org-notes/).

These are my set of personal notes, created using [Org Mode](http:www.orgmode.org). If you have accidentally stumbled across this site, mind the following disclaimer:

DISCLAIMER: I am a student, and these notes are part of my learning process. I do not intend for them to be used as a source or reference to supplement other people's work. I make no claim about their quality or accuracy. Read at your own discretion.
